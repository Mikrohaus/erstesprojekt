from django.contrib import admin

# Register your models here.
from .models import Liste, Produkt
#admin.site.register(Category)
#admin.site.register(Todo)

# admin.site.register(Liste)
# admin.site.register(Produkt)

class ProduktInline(admin.TabularInline):
    model = Produkt
class ListeAdmin(admin.ModelAdmin): 
    inlines = [ProduktInline]

admin.site.register(Liste, ListeAdmin) 