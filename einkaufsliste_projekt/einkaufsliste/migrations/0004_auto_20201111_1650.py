# Generated by Django 2.2.5 on 2020-11-11 15:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('einkaufsliste', '0003_auto_20201110_1950'),
    ]

    operations = [
        migrations.AlterField(
            model_name='produkt',
            name='menge',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='produkt',
            name='preis',
            field=models.FloatField(default=0.0),
        ),
    ]
