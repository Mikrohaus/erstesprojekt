# Generated by Django 2.2.5 on 2020-11-11 21:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('einkaufsliste', '0005_auto_20201111_2143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='produkt',
            name='name',
            field=models.TextField(),
        ),
    ]
