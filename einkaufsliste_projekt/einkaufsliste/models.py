from django.db import models

# Create your models here.
class Liste(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name}"


class Produkt(models.Model):

    name = models.TextField(blank=False, null=False)
    preis = models.FloatField(blank=False, null=False, default=0.0)
    menge = models.IntegerField(blank=False, null=False, default=1)
    status = models.BooleanField(default=False)
    liste = models.ForeignKey(to="Liste", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} {self.liste} {self.status} {self.preis} {self.menge}"