from django.urls import path
from . import views



urlpatterns = [ 
    path('', views.overview, name="overview"),
    path('liste/<int:liste_id>', views.liste, name="liste"),
    path("liste_anlegen", views.liste_anlegen, name="liste_anlegen"),
    path("produkt_anlegen/<int:liste_id>", views.produkt_anlegen, name="produkt_anlegen"),
    path("produkt_bearbeiten/<int:produkt_id>", views.produkt_bearbeiten, name="produkt_bearbeiten"),
    path("produkt_loeschen/<int:liste_id>/<int:produkt_id>", views.produkt_loeschen, name="produkt_loeschen"),
    path("liste_loeschen/<int:liste_id>", views.liste_loeschen, name="liste_loeschen"),
    path("switch_status/<int:liste_id>/<int:produkt_id>", views.switch_status, name="switch_status"),
    
]