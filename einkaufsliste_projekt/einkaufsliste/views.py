from django.shortcuts import render, get_object_or_404, reverse, redirect
from django.http import HttpResponseRedirect
from django.template.defaulttags import register
from . import models
# Create your views here.


def overview(request):
  listen = models.Liste.objects.all()
  context = {"listen": listen}
  return render(request, 'einkaufsliste/overview.html', context)


def liste(request, liste_id):
  
  liste= get_object_or_404(models.Liste, pk=liste_id)
  produkte = models.Produkt.objects.filter(liste=liste_id)
  summe = 0
  for produkt in produkte:
    summe += produkt.preis * produkt.menge
  gesamt = {}
  for produkt in produkte:
    gesamt[produkt.name] = produkt.preis * produkt.menge
  context = {"liste":liste, "produkte":produkte, "summe":summe, "gesamt":gesamt}
  return render(request, "einkaufsliste/liste.html", context)


def liste_anlegen(request):
  models.Liste.objects.create(name=request.POST["liste_name"])
  # liste= get_object_or_404(models.Liste, name=request.POST["liste_name"])
  # models.Produkt.objects.create(name="Erster Eintrag", liste=liste)
  return redirect("overview")



def switch_status(request, liste_id, produkt_id):
  produkt = get_object_or_404(models.Produkt, pk=produkt_id)
  print(produkt)
  produkt.status = not produkt.status
  produkt.save()
  return redirect("http://localhost:8000/einkaufsliste/liste/"+ str(liste_id))
    


def produkt_anlegen(request, liste_id):
  liste = get_object_or_404(models.Liste, pk=liste_id)
  #Dublettenprüfung und neues anlegen
  
    
  produkte = models.Produkt.objects.filter(liste=liste_id)
  print(produkte)
  if not produkte.count():
    models.Produkt.objects.create(name=request.POST["produkt_name"], liste=liste, preis=request.POST["produkt_preis"], menge=request.POST["produkt_menge"])
  else: 
    for produkt in produkte:
      produkt.name      
      if produkt.name == request.POST["produkt_name"]:
        return redirect("liste", liste_id)        
      else: 
        models.Produkt.objects.create(name=request.POST["produkt_name"], liste=liste, preis=request.POST["produkt_preis"], menge=request.POST["produkt_menge"])
        return redirect("liste", liste_id)
  
  return redirect("liste", liste_id)


def produkt_bearbeiten(request, produkt_id):
  produkt = get_object_or_404(models.Produkt, pk=produkt_id)
  listen = models.Liste.objects.all()
  liste = produkt.liste
  if request.POST:
    produkt.name = request.POST["produkt_name"]
    produkt.preis = request.POST["produkt_preis"]
    produkt.menge = request.POST["produkt_menge"]
    produkt.status = "produkt_status" in request.POST
    produkt.liste = get_object_or_404(models.Liste, pk=request.POST["produkt_liste"])
  produkt.save()
  
  context = {"listen": listen, "produkt":produkt}
  return render(request, "einkaufsliste/produkt_bearbeiten.html", context)

def produkt_loeschen(request, liste_id, produkt_id):
  produkt = get_object_or_404(models.Produkt, pk=produkt_id)
  if request.method == "POST":
        produkt.delete()
        return redirect("http://localhost:8000/einkaufsliste/liste/"+ str(liste_id))
  return redirect("http://localhost:8000/einkaufsliste/liste/"+ str(liste_id))


def liste_loeschen(request, liste_id):
  liste = get_object_or_404(models.Liste, pk=liste_id)
  if request.method == "POST":
        liste.delete()
        return redirect("overview")
  return redirect("overview")




@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)