# Generated by Django 2.2.5 on 2020-12-02 10:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('textadventure', '0014_savegame_save_items'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventoryitem',
            name='text_take',
            field=models.TextField(default='kein Text bisher'),
        ),
    ]
