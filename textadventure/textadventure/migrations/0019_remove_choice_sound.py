# Generated by Django 2.2.5 on 2020-12-02 11:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('textadventure', '0018_auto_20201202_1156'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='choice',
            name='sound',
        ),
    ]
