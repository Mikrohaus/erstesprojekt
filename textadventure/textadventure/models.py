from django.db import models

# Create your models here.


class Station(models.Model):
    '''
    Eine Station im Textadventure. Der Name ist eine sprechende ID,
    muss also eindeutig (unique) sein.
    '''
    name = models.CharField(max_length=50, unique=True)
    station_probability = models.BooleanField(default=False)
    title = models.CharField(max_length=200)
    text = models.TextField()
    image = models.TextField(null=True, default="textadventure/256x256.png")
    music = models.TextField(null=True, default="textadventure/adventure.mp3")
    def __str__(self):
        return self.name

class InventoryItem(models.Model):
    name = models.CharField(max_length=100)
    text = models.TextField()
    text_take = models.TextField(default="kein Text bisher")
    station = models.ForeignKey(to=Station, on_delete=models.CASCADE)
    taken = models.BooleanField(default=False)
    
    def __str__(self):
        return f"{self.name} {self.text} {self.station} {self.taken} {self.text_take}"


class Choice(models.Model):
    '''
    Eine Auswahloptin. Neben dem Text zur Beschreibung enthält Sie auch den 
    Verweis auf die nächste Station.
    '''
    station = models.ForeignKey(to=Station, on_delete=models.CASCADE, related_name="choice_set")
    text = models.CharField(max_length=200)
    next_station = models.ForeignKey(to=Station, on_delete=models.CASCADE, related_name="incoming_set")
    item_required_station = models.ForeignKey(to=Station, on_delete=models.CASCADE, blank=True, null=True, related_name="incoming_required_set")
    item_required = models.ForeignKey(to=InventoryItem, on_delete=models.CASCADE, blank=True, null=True)
    def __str__(self):
        return f"{self.text} {self.next_station.name} {self.item_required} " 


class Probability(models.Model):
    choice = models.ForeignKey(to=Choice, on_delete=models.CASCADE)
    probability = models.FloatField()
    def __str__(self):
        return f"{self.choice} {self.probability}" 


class Savegame(models.Model):
    name = models.CharField(max_length=50)
    save_station = models.CharField(max_length=50)
    save_items = models.TextField(null=True)
    def __str__(self):
        return f"{self.name} {self.save_station} {self.save_items}"


