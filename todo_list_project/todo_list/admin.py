from django.contrib import admin

# Register your models here.
from .models import Category, Todo
#admin.site.register(Category)
#admin.site.register(Todo)

#admin.site.register(Category)
#admin.site.register(Todo)

class TodoInline(admin.TabularInline):
    model = Todo
class CategoryAdmin(admin.ModelAdmin): 
    inlines = [TodoInline]

admin.site.register(Category, CategoryAdmin) 