from django.urls import path
from . import views


app_name = "todo_list" 
urlpatterns = [ 
    path('', views.overview, name="overview"),
    path('<int:todo_id>/', views.detail, name="detail"),
    path('<int:todo_id>/status_change', views.status_change, name="status_change"),
    path('new', views.new, name="new"),
]